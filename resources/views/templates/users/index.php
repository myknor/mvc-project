<div class="content">
    <div class="title">
        <h4>Vartotojai</h4>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Vardas ir Pavardė</th>
            <th scope="col">Gimimo data</th>
            <th scope="col">Veiksmai</th>
        </thead>
        <tr>
            <tbody>
            <?php
            foreach ($content['attributes'] as $key => $attribute) {
                echo "<tr>
                <th scope='row'>" . ($key + 1) . "</th>
                <td><a href='/users/$attribute[id]'>$attribute[first_name] $attribute[last_name]</td>
                <td>$attribute[date_of_birth]</td>
                <td>
                <a href='http://practice_2.test/users/" . $attribute['id'] . "/edit' type='button' class='btn btn-info'>Keisti</a>
                <button type='button' class='delete btn btn-danger' data-model = 'users' data-id='$attribute[id]'>Pašalinti</button></td>
              </tr>";
            }
            ?>
            </tbody>
    </table>
</div>