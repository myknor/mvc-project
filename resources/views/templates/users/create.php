<div class="content">
    <div class="title">
        <h4>Sukurti vartotoją</h4>
    </div>
    <form method="POST" action="<?= host() . '/users/store';  ?>">
        <div class="form-group">
            <label for="first_name">Vartotojo vardas</label>
            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Vardas">
        </div>
        <div class="form-group">
            <label for="last_name">Vartotojo pavardė</label>
            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Pavardė">
        </div>
        <div class="form-group">
            <label for="date_of_birth">Gimimo data</label>
            <input type="text" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="Gimimo data">
        </div>
        <button type="submit" class="btn btn-primary">Patvirtinti</button>
    </form>
</div>