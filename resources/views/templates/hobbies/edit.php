<div class="content">
    <div class="title">
        <h4>Keisti pomėgį</h4>
    </div>
    <form method="POST" action="<?= host() . '/hobbies/update';  ?>">
        <input type="hidden" name="id" value="<?=$content['attributes']['id']?>">
        <div class="form-group">
            <label for="name">Pomėgio pavadinimas</label>
            <input type="text" class="form-control" name="name" id="name" value="<?=$content['attributes']['name']?>" placeholder="Pomėgis">
        </div>
        <button type="submit" class="btn btn-primary">Patvirtinti</button>
    </form>
</div>