<div class="content">
    <div class="title">
        <h4>Pomėgiai</h4>
    </div>
    <table class="table table-striped">
        <thead>
        <th scope="col">#</th>
        <th scope="col">Pavadinimas</th>
        <th scope="col">Sukūrimo data</th>
        <th scope="col">Atnaujinimo data</th>
        <th scope="col">Veiksmai</th>
        </thead>
        <tbody>
        <?php
        foreach ($content['attributes'] as $key => $attribute) {
            echo "<tr>
                <th scope='row'>" . ($key + 1) . "</th>
                <td><a href='/users/$attribute[id]'>$attribute[name]</td>
                <td>$attribute[created_at]</td>
                <td>$attribute[updated_at]</td>
                <td>
                <a href='http://practice_2.test/hobbies/" . $attribute['id'] . "/edit' type='button' class='btn btn-info'>Keisti</a>
                <button type='button' class='delete btn btn-danger' data-model = 'hobbies' data-id='$attribute[id]'>Pašalinti</button></td>
              </tr>";
        }
        ?>
        </tbody>
    </table>
</div>