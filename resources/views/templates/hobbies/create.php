<div class="content">
    <div class="title">
        <h4>Sukurti pomėgį</h4>
    </div>
    <form method="POST" action="<?= host() . '/hobbies/store';  ?>">
        <div class="form-group">
            <label for="name">Pomėgio pavadinimas</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Pomėgis">
        </div>
        <button type="submit" class="btn btn-primary">Patvirtinti</button>
    </form>
</div>