<?php
use App\Core\Router;
use \App\Core\Request;

$request = new Request();
$Router = new Router($request);

//home
$Router->get('/',['\App\Controllers\HomeController','index']);

//users
$Router->get('/users',['\App\Controllers\UserController','index']);
$Router->get('/users/{id}',['\App\Controllers\UserController','show']);
$Router->get('/users/{id}/edit',['\App\Controllers\UserController','edit']);
$Router->get('/users/create',['\App\Controllers\UserController','create']);
$Router->post('/users/store',['\App\Controllers\UserController','store']);
$Router->post('/users/update',['\App\Controllers\UserController','update']);
$Router->delete('/users/delete/{id}',['\App\Controllers\UserController','delete']);

//hobbies
$Router->get('/hobbies',['\App\Controllers\HobbyController','index']);
$Router->get('/hobbies/{id}',['\App\Controllers\HobbyController','show']);
$Router->get('/hobbies/{id}/edit',['\App\Controllers\HobbyController','edit']);
$Router->get('/hobbies/create',['\App\Controllers\HobbyController','create']);
$Router->post('/hobbies/store',['\App\Controllers\HobbyController','store']);
$Router->post('/hobbies/update',['\App\Controllers\HobbyController','update']);
$Router->delete('/hobbies/delete/{id}',['\App\Controllers\HobbyController','delete']);

$Router->resolve();