<?php

namespace App\Models;

use App\Traits\Connections;
use Carbon\Carbon;

abstract class Model
{
    use Connections;

    protected $table;
    public $attributes;

    public function __construct()
    {
        $this->boot();
    }

    private function boot()
    {
        $this->databaseInit();
    }

    public function find($id)
    {
        $this->conditions('id', '=', $id)->get();
        return $this;
    }

    public function get()
    {
        $this->conditions('deleted_at','IS', 'NULL');
        $this->setAttributes($this->select($this->table)->runQuery());
        return $this;
    }

    public function where($column, $operator, $value)
    {
        $this->conditions($column, $operator, $value);
        return $this;
    }

    public function create(array $data)
    {
        try {
            $this->insert($data);
        } catch (\PDOException $e) {
            throw $e;
        }

        return $this->connection->lastInsertId();
    }

    public function softDelete($id)
    {
        try {
            $date = Carbon::now();
            $this->conditions('id', '=', $id);
            $this->sqlUpdate(['deleted_at' => $date->toDateTimeString()]);
        } catch (\PDOException $e) {
            throw $e;
        }

        return true;
    }

    public function update($data, $id){
        try {
            $this->conditions('id', '=', $id);
            $this->sqlUpdate($data);
        } catch (\PDOException $e) {
            throw $e;
        }

        return true;
    }


    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }
}