<?php

namespace App\View;

class View
{
    function render($view, array $content = [], $base_view = 'base')
    {
        require_once dirname(__DIR__) . "\\..\\resources\\views\\templates\\{$base_view}.php";
    }

}