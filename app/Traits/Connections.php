<?php

namespace App\Traits;

use PDO;

trait Connections
{
    private string $config;
    private string $db;
    private string $host;
    private string $user;
    private string $password;
    private $connection;
    private $query;
    private $conditions = '';

    private static function configData()
    {
        $config = include dirname(__DIR__) . '\\..\\config\\' . 'connections.php';
        return $config['database']['mysql'];
    }

    private function databaseInit()
    {
        $this->loadParameters();
        $this->pdoConnect();
    }

    public function loadParameters()
    {
        $config = static::configData();

        $this->db = $config['db_name'];
        $this->host = $config['host'];
        $this->user = $config['user'];
        $this->password = $config['password'];
    }

    private function pdoConnect()
    {
        $d = "mysql:host=$this->host;dbname=$this->db";
        $this->connection = new PDO($d, $this->user, $this->password);
    }

    private function conditions($column, $operator, $value)
    {
        if (!strpos($this->conditions, 'WHERE')) {
            $conditional = ' WHERE ';
        } else {
            $conditional =  ' AND ';
        }

        $this->conditions .= ' ' . $conditional . ' ' . '`' . $column . '`'.  ' ' . $operator . ' ' . $value;

        return $this;
    }

    private function select($table, $selections = '*')
    {
        $sql = 'SELECT ' . $selections . ' FROM ' . $table;
        $sql = $sql . $this->conditions;
        $this->query = $this->connection->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        return $this;
    }

    private function insert(array $data)
    {
        $columns = [];

        foreach ($data as $column => $param) {
            $values[] = ":$column";
            $columns[] = "$column";
        }

        $values = implode(', ', $values);
        $columns = implode(', ', $columns);

//      'INSERT INTO users  (first_name,last_name,date_of_birth) VALUES (:first_name,:last_name,:date_of_birth');
        $sql = "INSERT INTO $this->table  ($columns) VALUES ($values)";

        $this->query = $this->connection->prepare($sql);
        $this->query->execute($data);

        return $this;
    }

    private function sqlUpdate(array $data)
    {
        //UPDATE user set (first_name = :first_name, last_name = :last_name) WHERE id = 1
        foreach ($data as $column => $param) {
            $columnValues[] = "$column = :$column";
        }

        $columnValues = implode(', ', $columnValues);
        $sql = "UPDATE $this->table SET $columnValues";
        $sql = $sql . $this->conditions;

        $this->query = $this->connection->prepare($sql);
        $this->query->execute($data);

        return $this;
    }

    public function runQuery($data = null)
    {
        $this->query->execute();
        return $this->query->fetchAll(PDO::FETCH_ASSOC);
    }

}