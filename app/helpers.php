<?php

if (!function_exists('host')) {
    function host() :string
    {
        $request = $GLOBALS['request'];
        return $request->getHost();
    }
}