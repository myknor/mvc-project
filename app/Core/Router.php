<?php

namespace App\Core;

class Router
{
    private $uri;

    private $loadedRoutes;

    private Request $request;

    public function __construct(Request $request)
    {
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->request = $request;
    }

    //
    public function get($path, array $controller)
    {
        $this->loadedRoutes['get'][$path] = $controller;
    }

    public function post($path, array $controller)
    {
        $this->loadedRoutes['post'][$path] = $controller;
    }

    public function delete($path, array $controller)
    {
        $this->loadedRoutes['delete'][$path] = $controller;
    }

    public function resolve()
    {
        $path = $this->request->getPath();
        $method = $this->request->getMethod();
        $callback = $this->loadedRoutes[$method][$path] ?? false;

        if ($callback === false) {
            $best_match = $this->matchRoutes(explode('/', $path), array_keys($this->loadedRoutes[$method]));
            $callback = $this->loadedRoutes[$method][$best_match];

            if ($callback) {
                $variable = $this->getRouteVariable($best_match,$path);
            }
        }

        if ($callback === false) {
            echo 'Page not found';
        } elseif (is_array($callback)) {

            include_once dirname(__DIR__) . '\\..\\' . $callback[0] . '.php';
            $controller = $callback[0];
            $instance = new $controller();

            return call_user_func_array([$instance, $callback[1]],[$variable ?? $this->request]);
        }
    }

    public function matchRoutes(array $path, $routes)
    {
        $curlies = $this->filterCurlies($routes);

        $matches = [];

        foreach ($curlies as $curly_key => $curly) {
            $curly = explode('/', $curly);
            if (count($curly) === count($path)) {
                foreach ($curly as $key => $c) {
                    if (isset($path[$key])) {
                        if ($path[$key] === $c && $c !== "") {
                            $matches[$curly_key][] = $c;
                        }
                    }
                }
            }
        }

        $count = 0; $best_match_key = '';
        //find the route with the most matched elements
        foreach($matches as $key => $match){
            if($count<=count($match)){
                $count = count($match);
                $best_match_key = $key;
            }
        }

        return $curlies[$best_match_key];

    }

    function filterCurlies($routes)
    {
        foreach ($routes as $route) {
            if (strstr($route, '{') && strstr($route, '}')) {
                $curlies[] = $route;
            }
        }
        return $curlies;
    }

    function getRouteVariable($best_match,$path)
    {
        $first = strpos($best_match, '{');

        $value = substr($path, $first);
        $slash = strpos($value,'/',0);
        $value = $slash ? substr($value,0,$slash) : $value;

        return $value;
    }

}