<?php

namespace App\Core;

class Request
{
    public function __construct()
    {
        $GLOBALS['request'] = $this;
    }

    public function getPath()
    {
        $path = $_SERVER['REQUEST_URI'] ?? "/";

        $position = strpos($path,'?');
        if($position === false){
            return $path;
        }

        return substr($path, 0, $position);
    }

    public function getMethod()
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    public function all(){
        return $_POST ?? $_GET;
    }

    public function getHost()
    {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
    }
}