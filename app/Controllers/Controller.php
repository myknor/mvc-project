<?php

namespace App\Controllers;

use App\View\View;

abstract class Controller
{
    public $view;

    function __construct()
    {
        $this->view = new View();
    }

    private function init(){
        $this->init();
    }
}