<?php

namespace App\Controllers;

class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $content = ['welcome'=>'Welcome home'];
        $view = '\\home\\index';
        $this->view->render($view, $content);
    }
}