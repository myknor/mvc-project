<?php

namespace App\Controllers;

use App\Core\Request;
use App\Models\Hobby;

class HobbyController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $hobby = (new Hobby())->get();
        $content = [
            'attributes' => $hobby->attributes
        ];

        $view = '\\hobbies\\index';
        $this->view->render($view, $content);

    }

    public function show($id)
    {
        var_dump('we are in show', $id);
        die();
    }

    public function create()
    {
        $view = '\\hobbies\\create';
        $this->view->render($view);
    }

    public function edit($id)
    {

        $hobby = (new Hobby())->find($id);
        $content = [
            'attributes' => $hobby->attributes[0]
        ];
        $view = '\\hobbies\\edit';
        $this->view->render($view, $content);

    }

    public function update(Request $request)
    {
        $all = $request->all();
        $id = $all['id'];
        unset($all['id']);


        $result = (new Hobby())->update($all,$id);
        if($result){
            $content = 'Pomėgio duomenys pakeisti!';
            $type = 'success';
        }else{
            $content = 'Nepavyko atnaujinti pomėgio duomenų';
            $type = 'danger';
        }

        $message = json_encode(['content'=>$content, 'type'=>$type]);
        header("Location: ". host() . "/hobbies?message=$message");
    }

    public function store(Request $request)
    {
        $all = $request->all();

        $result = (new Hobby())->create($all);
        if($result){
            $content = 'Pomėgis sukurtas';
            $type = 'success';
        }else{
            $content = 'Nepavyko sukurti pomėgio';
            $type = 'danger';
        }
        $message = json_encode(['content'=>$content, 'type'=>$type]);
        header("Location: ". host() . "/hobbies?message=$message");
    }

    public function delete($id)
    {
        $result = (new Hobby())->softDelete($id);
        return json_encode([
            'status' => ($result ?: false)
        ]);
    }

}