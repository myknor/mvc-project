<?php

namespace App\Controllers;

use App\Core\Request;
use App\Models\User;
use DateTime;

class UserController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $user = (new User())->get();
        $content = [
            'attributes' => $user->attributes
        ];

        $view = '\\users\\index';
        $this->view->render($view, $content);
    }

    public function show($id)
    {
        var_dump('we are in show', $id);
        die();
    }

    public function create()
    {
        $view = '\\users\\create';
        $this->view->render($view);
    }

    public function edit($id)
    {
        $user = (new User())->find($id);
        $content = [
            'attributes' => $user->attributes[0]
        ];

        $view = '\\users\\edit';
        $this->view->render($view, $content);
    }

    public function update(Request $request)
    {
        $all = $request->all();
        $id = $all['id'];
        unset($all['id']);
        $date = $all['date_of_birth'];

        $validate = ($date && DateTime::createFromFormat('Y-m-d', $date)) ? true : false;

        if (!$validate) {
            $message = json_encode(['content'=>'Netinkamas datos formatas!','type'=>'danger']);
            header("Location: ". host() . "/users/$id/edit?message=$message");
            return;
        }



        $result = (new User())->update($all,$id);
        if($result){
            $content = 'Vartotojo duomenys pakeisti!';
            $type = 'success';
        }else{
            $content = 'Nepavyko atnaujinti vartotojo duomenų';
            $type = 'danger';
        }

        $message = json_encode(['content'=>$content, 'type'=>$type]);
        header("Location: ". host() . "/users?message=$message");
    }

    public function store(Request $request)
    {
        $all = $request->all();
        $date = $all['date_of_birth'];

        $validate = ($date && DateTime::createFromFormat('Y-m-d', $date)) ? true : false;

        if (!$validate) {
            $message = json_encode(['content'=>'Netinkamas datos formatas!','type'=>'danger']);
            header("Location: ". host() . "/users/create?message=$message");
            return;
        }

        $result = (new User())->create($all);
        if($result){
            $content = 'Vartotojas sukurtas';
            $type = 'success';
        }else{
            $content = 'Nepavyko sukurti vartotojo';
            $type = 'danger';
        }
        $message = json_encode(['content'=>$content, 'type'=>$type]);
        header("Location: ". host() . "/users?message=$message");
    }

    public function delete($id)
    {
        $result = (new User())->softDelete($id);
        return json_encode([
            'status' => ($result ?: false)
        ]);
    }

}