$(function () {
    $(document).on('click', '.delete', function () {
        let $this = $(this),
            id = $this.data('id'),
            model = $this.data('model');

        $.ajax({
            url: "http://practice_2.test/" + model + "/delete/" + id,
            type: 'DELETE',
            contentType: 'application/json',
            success: function (result) {
                if (!result) {
                    $this.closest('tr').remove();
                } else {
                    alert('Operacija nepavyko');
                }
            }
        });
    });
    $(document).on('click', '.alert', function () {
        $(this).closet('div').remove
    });
});